router.route('/createGearCategory').post(function(req, res) {

    var id = parsedJsonData(req.body.id);
    var name = parsedJsonData(req.body.name);

    try {
        con.connect(function(err) {
            if (err) throw err;
            con.query("insert into gear_category (name) values (" +
                name + ")",
                function(err, result, fields) {
                    if (err) throw err;
                    res.json({
                        users: result
                    });
                });
        });
    } catch (err) {
        console.log(err);
    }
});

router.route('/getGearCategory').get(function(req, res) {
    try {
        con.connect(function(err) {
            if (err) throw err;
            con.query("SELECT * FROM gear_category", function(err, result, fields) {
                if (err) throw err;
                res.json({
                    users: result
                });
            });
        });
    } catch (err) {
        console.log(err);
    }
});

router.route('/updateGearCategory').post(function(req, res) {

    var id = parsedJsonData(req.body.id);
    var name = parsedJsonData(req.body.name);

    var sql = "UPDATE gear_category SET ";

    if (name != "NULL") {
        sql = sql + "name=" + name + ",";
    }

    var sql = sql + "WHERE id=" + id;

    try {
        con.connect(function(err) {
            if (err) throw err;
            console.log(sql);
            con.query(sql,
                function(err, result, fields) {
                    if (err) throw err;
                    res.json({
                        users: result
                    });
                });
        });
    } catch (err) {
        console.log(err);
    }
});

router.route('/deleteGearCategory').post(function(req, res) {

    var id = parsedJsonData(req.body.id);

    try {
        con.connect(function(err) {
            if (err) throw err;
            con.query("DELETE FROM gear_category WHERE id=" + id, function(err, result, fields) {
                if (err) throw err;

                res.json({
                    users: result
                });
            });
        });
    } catch (err) {
        console.log(err);
    }
});
