router.route('/createGear').post(function(req, res) {

    var id = parsedJsonData(req.body.id);
    var cat_id = parsedJsonData(req.body.cat_id);
    var client_id = parsedJsonData(req.body.client_id);
    var status = parsedJsonData(req.body.status);

    try {
        con.connect(function(err) {
            if (err) throw err;
            con.query("insert into gear (cat_id, client_id, status) values (" +
                cat_id + ", " + client_id + ", " + status + ")",
                function(err, result, fields) {
                    if (err) throw err;
                    res.json({
                        users: result
                    });
                });
        });
    } catch (err) {
        console.log(err);
    }
});

router.route('/getGear').get(function(req, res) {
    try {
        con.connect(function(err) {
            if (err) throw err;
            con.query("SELECT * FROM gear", function(err, result, fields) {
                if (err) throw err;
                res.json({
                    users: result
                });
            });
        });
    } catch (err) {
        console.log(err);
    }
});

router.route('/updateGear').post(function(req, res) {

    var id = parsedJsonData(req.body.id);
    var cat_id = parsedJsonData(req.body.cat_id);
    var client_id = parsedJsonData(req.body.client_id);
    var status = parsedJsonData(req.body.status);

    var sql = "UPDATE gear SET ";

    if (cat_id != "NULL") {
        sql = sql + "cat_id=" + cat_id + ",";
    }

    if (client_id != "NULL") {
        sql = sql + "client_id=" + client_id + ",";
    }

    if (status != "NULL") {
        sql = sql + "status=" + status + ",";
    }

    var sql = sql + "WHERE id=" + id;

    try {
        con.connect(function(err) {
            if (err) throw err;
            console.log(sql);
            con.query(sql,
                function(err, result, fields) {
                    if (err) throw err;
                    res.json({
                        users: result
                    });
                });
        });
    } catch (err) {
        console.log(err);
    }
});

router.route('/deleteGear').post(function(req, res) {

    var id = parsedJsonData(req.body.id);

    try {
        con.connect(function(err) {
            if (err) throw err;
            con.query("DELETE FROM gear WHERE id=" + id, function(err, result, fields) {
                if (err) throw err;

                res.json({
                    users: result
                });
            });
        });
    } catch (err) {
        console.log(err);
    }
});
