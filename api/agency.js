router.route('/createAgency').post(function(req, res) {

    var id = parsedJsonData(req.body.id);
    var name = parsedJsonData(req.body.name);
    var address = parsedJsonData(req.body.address);

    try {
        con.connect(function(err) {
            if (err) throw err;
            con.query("insert into agencies (name, address) values (" +
                name + ", " + address + ")",
                function(err, result, fields) {
                    if (err) throw err;
                    res.json({
                        users: result
                    });
                });
        });
    } catch (err) {
        console.log(err);
    }
});

router.route('/getAgencies').get(function(req, res) {
    try {
        con.connect(function(err) {
            if (err) throw err;
            con.query("SELECT * FROM agencies", function(err, result, fields) {
                if (err) throw err;
                res.json({
                    users: result
                });
            });
        });
    } catch (err) {
        console.log(err);
    }
});

router.route('/updateAgency').post(function(req, res) {

    var id = parsedJsonData(req.body.id);
    var name = parsedJsonData(req.body.name);
    var address = parsedJsonData(req.body.address);

    var sql = "UPDATE agencies SET ";

    if (name != "NULL") {
        sql = sql + "name=" + name + ",";
    }

    if (address != "NULL") {
        sql = sql + "address=" + address + ",";
    }

    var sql = sql + "WHERE id=" + id;

    try {
        con.connect(function(err) {
            if (err) throw err;
            console.log(sql);
            con.query(sql,
                function(err, result, fields) {
                    if (err) throw err;
                    res.json({
                        users: result
                    });
                });
        });
    } catch (err) {
        console.log(err);
    }
});

router.route('/deleteAgency').post(function(req, res) {

    var id = parsedJsonData(req.body.id);

    try {
        con.connect(function(err) {
            if (err) throw err;
            con.query("DELETE FROM agencies WHERE id=" + id, function(err, result, fields) {
                if (err) throw err;

                res.json({
                    users: result
                });
            });
        });
    } catch (err) {
        console.log(err);
    }
});
