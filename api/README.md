# API Instructions 

## Users
### Get All Users

GET http://71.19.254.108:8080/getUsers

### Create User

POST http://71.19.254.108:8080/createUser - {"type":1, "status":"Approved", "password":"jfds2232", "added_by":"Rob", "date_created":"2018-09-22", "email":"rob@duffy.tech", "phone":"6044428741", "name":"bob"}

### Get single user by ID

POST http://71.19.254.108:8080/getUserById - {"id":"1"}

### Update User

POST http://71.19.254.108:8080/udpateUser - {"id":"1", "type":1, "status":"Approved", "password":"jfds2232", "added_by":"Rob", "date_created":"2018-09-22", "email":"rob@duffy.tech", "phone":"6044428741", "name":"bob"}

### Delete User

POST http://71.19.254.108:8080/deleteUser - {"id":"1"}


## Clients
### Get All Clients

GET http://71.19.254.108:8080/getClients

### Create Client

POST http://71.19.254.108:8080/createClient - {"type":1, "firstName":"Bob", "lastName":"Star", "dateOfBirth":"1988-09-22", "email":"rob@duffy.tech", "phone":"6044428741", "name":"bob"}

### Get single client by ID

POST http://71.19.254.108:8080/getClientById - {"id":"1"}

### Approve Client *

POST http://71.19.254.108:8080/updateClient - {"id":"1"}

### Reject Client *

POST http://71.19.254.108:8080/updateClient - {"id":"1"}




### Update Client

POST http://71.19.254.108:8080/updateClient - {"id":"1", "type":1, "status":"Approved", "password":"jfds2232", "added_by":"Rob", "date_created":"2018-09-22", "email":"rob@duffy.tech", "phone":"6044428741", "name":"bob"}

### Delete Client

POST http://71.19.254.108:8080/deleteClient - {"id":"1"}


## Gear
### Get All Gear

GET http://71.19.254.108:8080/getGear

### Create Client

POST http://71.19.254.108:8080/createGear - {"type":1, "status":"Approved", "password":"jfds2232", "added_by":"Rob", "date_created":"2018-09-22", "email":"rob@duffy.tech", "phone":"6044428741", "name":"bob"}

### Get single client by ID

POST http://71.19.254.108:8080/getClientById - {"id":"1"}

### Update Client

POST http://71.19.254.108:8080/updateClient - {"id":"1", "type":1, "status":"Approved", "password":"jfds2232", "added_by":"Rob", "date_created":"2018-09-22", "email":"rob@duffy.tech", "phone":"6044428741", "name":"bob"}

### Delete Client

POST http://71.19.254.108:8080/deleteClient - {"id":"1"}