router.route('/createClient').post(function(req, res) {

    var id = parsedJsonData(req.body.id);
    var firstName = parsedJsonData(req.body.firstName);
    var lastName = parsedJsonData(req.body.lastName);
    var dateOfBirth = parsedJsonData(req.body.dateOfBirth);
    var babyDateOfBirth = parsedJsonData(req.body.babyDateOfBirth);
    var phone = parsedJsonData(req.body.phone);
    var email = parsedJsonData(req.body.email);
    var demographic = parsedJsonData(req.body.demographic);
    var status = parsedJsonData(req.body.status);
    var appointment_id = parsedJsonData(req.body.appointment_id);
    var referencing_id = parsedJsonData(req.body.referencing_id);

    try {
        con.connect(function(err) {
            if (err) throw err;
            con.query("insert into clients (firstName, lastName, dateOfBirth, babyDateOfBirth, phone, email, demographic, status, appointment_id, referencing_id) values (" +
                firstName + ", " + lastName + ", " + dateOfBirth + ", " + babyDateOfBirth + ", " + phone + "," + email + ", " + demographic + ", " + status + ", " + appointment_id + ", " + referencing_id + ")",
                function(err, result, fields) {
                    if (err) throw err;
                    res.json({
                        users: result
                    });
                });
        });
    } catch (err) {
        console.log(err);
    }
});

router.route('/getClients').get(function(req, res) {
    try {
        con.connect(function(err) {
            if (err) throw err;
            con.query("SELECT * FROM clients", function(err, result, fields) {
                if (err) throw err;
                res.json({
                    users: result
                });
            });
        });
    } catch (err) {
        console.log(err);
    }
});

router.route('/updateClient').post(function(req, res) {

    var id = parsedJsonData(req.body.id);
    var firstName = parsedJsonData(req.body.firstName);
    var lastName = parsedJsonData(req.body.lastName);
    var dateOfBirth = parsedJsonData(req.body.dateOfBirth);
    var babyDateOfBirth = parsedJsonData(req.body.babyDateOfBirth);
    var phone = parsedJsonData(req.body.phone);
    var email = parsedJsonData(req.body.email);
    var demographic = parsedJsonData(req.body.demographic);
    var status = parsedJsonData(req.body.status);
    var appointment_id = parsedJsonData(req.body.appointment_id);
    var referencing_id = parsedJsonData(req.body.referencing_id);

    var sql = "UPDATE clients SET ";

    if (firstName != "NULL") {
        sql = sql + "firstName=" + firstName + ",";
    }

    if (lastName != "NULL") {
        sql = sql + "lastName=" + lastName + ",";
    }

    if (dateOfBirth != "NULL") {
        sql = sql + "dateOfBirth=" + dateOfBirth + ",";
    }

    if (babyDateOfBirth != "NULL") {
        sql = sql + "babyDateOfBirth=" + babyDateOfBirth + ",";
    }

    if (phone != "NULL") {
        sql = sql + "phone=" + phone + ",";
    }

    if (email != "NULL") {
        sql = sql + "email=" + email + ",";
    }

    if (demographic != "NULL") {
        sql = sql + "demographic=" + demographic + ",";
    }

    if (status != "NULL") {
        sql = sql + "status=" + status + ",";
    }

    if (appointment_id != "NULL") {
        sql = sql + "appointment_id=" + appointment_id + ",";
    }

    if (referencing_id != "NULL") {
        sql = sql + "referencing_id=" + referencing_id;
    }

    var sql = sql + "WHERE id=" + id;

    try {
        con.connect(function(err) {
            if (err) throw err;
            console.log(sql);
            con.query(sql,
                function(err, result, fields) {
                    if (err) throw err;
                    res.json({
                        users: result
                    });
                });
        });
    } catch (err) {
        console.log(err);
    }
});

router.route('/deleteClient').post(function(req, res) {

    var id = parsedJsonData(req.body.id);

    try {
        con.connect(function(err) {
            if (err) throw err;
            con.query("DELETE FROM clients WHERE id=" + id, function(err, result, fields) {
                if (err) throw err;

                res.json({
                    users: result
                });
            });
        });
    } catch (err) {
        console.log(err);
    }
});
