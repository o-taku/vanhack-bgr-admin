var mysql = require('mysql');
//require('dotenv').config();
var request = require('request');
var express = require('express');
var app = express();
var bodyParser = require('body-parser');

var db_login = {
    host: "localhost",
    user: "babygoround",
    password: "DFjsiofjpew#PPdfse2222jeiprwjfpi32jrESdf",
    database: "babygoround_api"
}

var con = mysql.createConnection(db_login);
var real_api_token = "klj4eoijpfoidsjf$@Edfijpsjfpppp";

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

var port = process.env.PORT || 8080;

var router = express.Router();

function parsedJsonData(data) {

    var json_data = JSON.stringify(data);
    if (json_data == "" || json_data == "Undefined" || json_data === undefined) {
        json_data = "NULL";
        return json_data;
    }

    var parsedData = JSON.parse(json_data);


    parsedData = "'" + parsedData + "'";
    return parsedData;
}

router.route('/createUser').post(function(req, res) {

    var api_token = parsedJsonData(req.body.type);
    var type = parsedJsonData(req.body.type);
    var status = parsedJsonData(req.body.status);
    var password = parsedJsonData(req.body.password);
    var added_by = parsedJsonData(req.body.added_by);
    var date_created = parsedJsonData(req.body.date_created);
    var agency_id = parsedJsonData(req.body.agency_id);
    var email = parsedJsonData(req.body.email);
    var phone = parsedJsonData(req.body.phone);
    var name = parsedJsonData(req.body.name);

    console.log("We just created a user named " + name);
    console.log("The agency id is " + agency_id);

    // Check the API token
    // if (real_api_token != api_token){
    //   res.send(500, { error: "The API token is invalid" });
    // }

    try {
        var con = mysql.createConnection(db_login);
        con.connect(function(err) {
            if (err) throw err;
            con.query("insert into users (type, status, password, added_by, date_created, agency_id, email, phone, name) values (0, " +
                status + ", " + password + ", " + added_by + ", " + date_created + ", " + agency_id + "," + email + ", " + phone + ", " + name + ")",
                function(err, result, fields) {
                    if (err) throw err;

                    //console.log(result);
                    //console.log(fields);
                    res.json({
                        users: result
                    });
                });
        });
    } catch (err) {
        console.log(err);

    }
});

router.route('/getUsers').get(function(req, res) {
    try {
        var con = mysql.createConnection(db_login);
        con.connect(function(err) {
            if (err) throw err;
            con.query("SELECT * FROM users", function(err, result, fields) {
                if (err) throw err;

                console.log(result);
                console.log(fields);
                res.json({
                    users: result
                });
            });
        });
    } catch (err) {
        console.log(err);
    }
});

router.route('/updateUser').post(function(req, res) {

    var id = parsedJsonData(req.body.id);
    var type = parsedJsonData(req.body.type);
    var status = parsedJsonData(req.body.status);
    var password = parsedJsonData(req.body.password);
    var added_by = parsedJsonData(req.body.added_by);
    var date_created = parsedJsonData(req.body.date_created);
    var agency_id = parsedJsonData(req.body.agency_id);
    var email = parsedJsonData(req.body.email);
    var phone = parsedJsonData(req.body.phone);
    var name = parsedJsonData(req.body.name);

    var sql = "UPDATE users SET ";

    if (type != "NULL") {
        sql = sql + "type=" + type + ",";
    }

    if (status != "NULL") {
        sql = sql + "status=" + status + ",";
    }

    if (password != "NULL") {
        sql = sql + "password=" + password + ",";
    }

    if (added_by != "NULL") {
        sql = sql + "added_by=" + added_by + ",";
    }

    if (date_created != "NULL") {
        sql = sql + "date_created=" + date_created + ",";
    }

    if (agency_id != "NULL") {
        sql = sql + "agency_id=" + agency_id + ",";
    }

    if (email != "NULL") {
        sql = sql + "email=" + email + ",";
    }

    if (phone != "NULL") {
        sql = sql + "phone=" + phone + ",";
    }

    if (name != "NULL") {
        sql = sql + "name=" + name;
    }

    var sql = sql + "WHERE id=" + id;

    console.log("We just created a user named " + name);
    console.log("The agency id is " + agency_id);
    try {
        var con = mysql.createConnection(db_login);
        con.connect(function(err) {
            if (err) throw err;
            console.log(sql);
            con.query(sql,
                function(err, result, fields) {
                    if (err) throw err;

                    //console.log(result);
                    //console.log(fields);
                    res.json({
                        users: result
                    });
                });
        });
    } catch (err) {
        console.log(err);
    }
});

router.route('/deleteUser').post(function(req, res) {

    var id = parsedJsonData(req.body.id);

    try {
        var con = mysql.createConnection(db_login);
        con.connect(function(err) {
            if (err) throw err;
            con.query("DELETE FROM users WHERE id=" + id, function(err, result, fields) {
                if (err) throw err;

                res.json({
                    users: result
                });
            });
        });
    } catch (err) {
        console.log(err);
    }
});

router.route('/createAgency').post(function(req, res) {

    var id = parsedJsonData(req.body.id);
    var name = parsedJsonData(req.body.name);
    var address = parsedJsonData(req.body.address);

    try {
        var con = mysql.createConnection(db_login);
        con.connect(function(err) {
            if (err) throw err;
            con.query("insert into agencies (name, address) values (" +
                name + ", " + address + ")",
                function(err, result, fields) {
                    if (err) throw err;
                    res.json({
                        agencies: result
                    });
                });
        });
    } catch (err) {
        console.log(err);
    }
});

router.route('/getAgencies').get(function(req, res) {
    try {
        var con = mysql.createConnection(db_login);
        con.connect(function(err) {
            if (err) throw err;
            con.query("SELECT * FROM agencies", function(err, result, fields) {
                if (err) throw err;
                res.json({
                    agencies: result
                });
            });
        });
    } catch (err) {
        console.log(err);
    }
});

router.route('/updateAgency').post(function(req, res) {

    var id = parsedJsonData(req.body.id);
    var name = parsedJsonData(req.body.name);
    var address = parsedJsonData(req.body.address);

    var sql = "UPDATE agencies SET ";

    if (name != "NULL") {
        sql = sql + "name=" + name + ",";
    }

    if (address != "NULL") {
        sql = sql + "address=" + address + ",";
    }

    var sql = sql + "WHERE id=" + id;

    try {
        var con = mysql.createConnection(db_login);
        con.connect(function(err) {
            if (err) throw err;
            console.log(sql);
            con.query(sql,
                function(err, result, fields) {
                    if (err) throw err;
                    res.json({
                        agencies: result
                    });
                });
        });
    } catch (err) {
        console.log(err);
    }
});

router.route('/deleteAgency').post(function(req, res) {

    var id = parsedJsonData(req.body.id);

    try {
        var con = mysql.createConnection(db_login);
        con.connect(function(err) {
            if (err) throw err;
            con.query("DELETE FROM agencies WHERE id=" + id, function(err, result, fields) {
                if (err) throw err;

                res.json({
                    agencies: result
                });
            });
        });
    } catch (err) {
        console.log(err);
    }
});

router.route('/createGearCategory').post(function(req, res) {

    var id = parsedJsonData(req.body.id);
    var name = parsedJsonData(req.body.name);

    try {
        var con = mysql.createConnection(db_login);
        con.connect(function(err) {
            if (err) throw err;
            con.query("insert into gear_category (name) values (" +
                name + ")",
                function(err, result, fields) {
                    if (err) throw err;
                    res.json({
                        gear_category: result
                    });
                });
        });
    } catch (err) {
        console.log(err);
    }
});

router.route('/getGearCategory').get(function(req, res) {
    try {
        var con = mysql.createConnection(db_login);
        con.connect(function(err) {
            if (err) throw err;
            con.query("SELECT * FROM gear_category", function(err, result, fields) {
                if (err) throw err;
                res.json({
                    gear_category: result
                });
            });
        });
    } catch (err) {
        console.log(err);
    }
});

router.route('/updateGearCategory').post(function(req, res) {

    var id = parsedJsonData(req.body.id);
    var name = parsedJsonData(req.body.name);

    var sql = "UPDATE gear_category SET ";

    if (name != "NULL") {
        sql = sql + "name=" + name + ",";
    }

    var sql = sql + "WHERE id=" + id;

    try {
        var con = mysql.createConnection(db_login);
        con.connect(function(err) {
            if (err) throw err;
            console.log(sql);
            con.query(sql,
                function(err, result, fields) {
                    if (err) throw err;
                    res.json({
                        users: result
                    });
                });
        });
    } catch (err) {
        console.log(err);
    }
});

router.route('/deleteGearCategory').post(function(req, res) {

    var id = parsedJsonData(req.body.id);

    try {
        var con = mysql.createConnection(db_login);
        con.connect(function(err) {
            if (err) throw err;
            con.query("DELETE FROM gear_category WHERE id=" + id, function(err, result, fields) {
                if (err) throw err;

                res.json({
                    users: result
                });
            });
        });
    } catch (err) {
        console.log(err);
    }
});

router.route('/createGear').post(function(req, res) {

    var id = parsedJsonData(req.body.id);
    var cat_id = parsedJsonData(req.body.cat_id);
    var client_id = parsedJsonData(req.body.client_id);
    var status = parsedJsonData(req.body.status);

    try {
        var con = mysql.createConnection(db_login);
        con.connect(function(err) {
            if (err) throw err;
            con.query("insert into gear (cat_id, client_id, status) values (" +
                cat_id + ", " + client_id + ", " + status + ")",
                function(err, result, fields) {
                    if (err) throw err;
                    res.json({
                        users: result
                    });
                });
        });
    } catch (err) {
        console.log(err);
    }
});

router.route('/getGear').get(function(req, res) {
    try {
        var con = mysql.createConnection(db_login);
        con.connect(function(err) {
            if (err) throw err;
            con.query("SELECT * FROM gear", function(err, result, fields) {
                if (err) throw err;
                res.json({
                    gear: result
                });
            });
        });
    } catch (err) {
        console.log(err);
    }
});

router.route('/updateGear').post(function(req, res) {

    var id = parsedJsonData(req.body.id);
    var cat_id = parsedJsonData(req.body.cat_id);
    var client_id = parsedJsonData(req.body.client_id);
    var status = parsedJsonData(req.body.status);

    var sql = "UPDATE gear SET ";

    if (cat_id != "NULL") {
        sql = sql + "cat_id=" + cat_id + ",";
    }

    if (client_id != "NULL") {
        sql = sql + "client_id=" + client_id + ",";
    }

    if (status != "NULL") {
        sql = sql + "status=" + status + ",";
    }

    var sql = sql + "WHERE id=" + id;

    try {
        var con = mysql.createConnection(db_login);
        con.connect(function(err) {
            if (err) throw err;
            console.log(sql);
            con.query(sql,
                function(err, result, fields) {
                    if (err) throw err;
                    res.json({
                        users: result
                    });
                });
        });
    } catch (err) {
        console.log(err);
    }
});

router.route('/deleteGear').post(function(req, res) {

    var id = parsedJsonData(req.body.id);

    try {
        var con = mysql.createConnection(db_login);
        con.connect(function(err) {
            if (err) throw err;
            con.query("DELETE FROM gear WHERE id=" + id, function(err, result, fields) {
                if (err) throw err;

                res.json({
                    users: result
                });
            });
        });
    } catch (err) {
        console.log(err);
    }
});

router.route('/createAppointment').post(function(req, res) {

    var id = parsedJsonData(req.body.id);
    var client_id = parsedJsonData(req.body.client_id);
    var appointment = parsedJsonData(req.body.appointment);
    var admin_id = parsedJsonData(req.body.admin_id);

    try {
        var con = mysql.createConnection(db_login);
        con.connect(function(err) {
            if (err) throw err;
            con.query("insert into appointments (client_id, appointment, admin_id) values (" +
                client_id + ", " + appointment + ", " + admin_id + ")",
                function(err, result, fields) {
                    if (err) throw err;
                    res.json({
                        users: result
                    });
                });
        });
    } catch (err) {
        console.log(err);
    }
});

router.route('/getAppointments').get(function(req, res) {
    try {
        var con = mysql.createConnection(db_login);
        con.connect(function(err) {
            if (err) throw err;
            con.query("SELECT * FROM appointments", function(err, result, fields) {
                if (err) throw err;
                res.json({
                    appointments: result
                });
            });
        });
    } catch (err) {
        console.log(err);
    }
});

router.route('/updateAppointment').post(function(req, res) {

    var id = parsedJsonData(req.body.id);
    var client_id = parsedJsonData(req.body.client_id);
    var appointment = parsedJsonData(req.body.appointment);
    var admin_id = parsedJsonData(req.body.admin_id);

    var sql = "UPDATE appointments SET ";

    if (client_id != "NULL") {
        sql = sql + "client_id=" + client_id + ",";
    }

    if (appointment != "NULL") {
        sql = sql + "appointment=" + appointment + ",";
    }

    if (admin_id != "NULL") {
        sql = sql + "admin_id=" + admin_id + ",";
    }

    var sql = sql + "WHERE id=" + id;

    try {
        var con = mysql.createConnection(db_login);
        con.connect(function(err) {
            if (err) throw err;
            console.log(sql);
            con.query(sql,
                function(err, result, fields) {
                    if (err) throw err;
                    res.json({
                        users: result
                    });
                });
        });
    } catch (err) {
        console.log(err);
    }
});

router.route('/deleteAppointment').post(function(req, res) {

    var id = parsedJsonData(req.body.id);

    try {
        var con = mysql.createConnection(db_login);
        con.connect(function(err) {
            if (err) throw err;
            con.query("DELETE FROM appointments WHERE id=" + id, function(err, result, fields) {
                if (err) throw err;

                res.json({
                    users: result
                });
            });
        });
    } catch (err) {
        console.log(err);
    }
});

router.route('/createClient').post(function(req, res) {

    var id = parsedJsonData(req.body.id);
    var firstName = parsedJsonData(req.body.firstName);
    var lastName = parsedJsonData(req.body.lastName);
    var dateOfBirth = parsedJsonData(req.body.dateOfBirth);
    var babyDateOfBirth = parsedJsonData(req.body.babyDateOfBirth);
    var phone = parsedJsonData(req.body.phone);
    var email = parsedJsonData(req.body.email);
    var demographic = parsedJsonData(req.body.demographic);
    var status = parsedJsonData(req.body.status);
    var appointment_id = parsedJsonData(req.body.appointment_id);
    var referencing_id = parsedJsonData(req.body.referencing_id);

    try {
        var con = mysql.createConnection(db_login);
        con.connect(function(err) {
            if (err) throw err;
            con.query("insert into clients (firstName, lastName, dateOfBirth, babyDateOfBirth, phone, email, demographic, status, appointment_id, referencing_id) values (" +
                firstName + ", " + lastName + ", " + dateOfBirth + ", " + babyDateOfBirth + ", " + phone + "," + email + ", " + demographic + ", " + status + ", " + appointment_id + ", " + referencing_id + ")",
                function(err, result, fields) {
                    if (err) throw err;
                    res.json({
                        users: result
                    });
                });
        });
    } catch (err) {
        console.log(err);
    }
});

router.route('/getClients').get(function(req, res) {
    try {
        var con = mysql.createConnection(db_login);
        con.connect(function(err) {
            if (err) throw err;
            con.query("SELECT * FROM clients", function(err, result, fields) {
                if (err) throw err;
                res.json({
                    clients: result
                });
            });
        });
    } catch (err) {
        console.log(err);
    }
});

router.route('/updateClient').post(function(req, res) {

    var id = parsedJsonData(req.body.id);
    var firstName = parsedJsonData(req.body.firstName);
    var lastName = parsedJsonData(req.body.lastName);
    var dateOfBirth = parsedJsonData(req.body.dateOfBirth);
    var babyDateOfBirth = parsedJsonData(req.body.babyDateOfBirth);
    var phone = parsedJsonData(req.body.phone);
    var email = parsedJsonData(req.body.email);
    var demographic = parsedJsonData(req.body.demographic);
    var status = parsedJsonData(req.body.status);
    var appointment_id = parsedJsonData(req.body.appointment_id);
    var referencing_id = parsedJsonData(req.body.referencing_id);

    var sql = "UPDATE clients SET ";

    if (firstName != "NULL") {
        sql = sql + "firstName=" + firstName + ",";
    }

    if (lastName != "NULL") {
        sql = sql + "lastName=" + lastName + ",";
    }

    if (dateOfBirth != "NULL") {
        sql = sql + "dateOfBirth=" + dateOfBirth + ",";
    }

    if (babyDateOfBirth != "NULL") {
        sql = sql + "babyDateOfBirth=" + babyDateOfBirth + ",";
    }

    if (phone != "NULL") {
        sql = sql + "phone=" + phone + ",";
    }

    if (email != "NULL") {
        sql = sql + "email=" + email + ",";
    }

    if (demographic != "NULL") {
        sql = sql + "demographic=" + demographic + ",";
    }

    if (status != "NULL") {
        sql = sql + "status=" + status + ",";
    }

    if (appointment_id != "NULL") {
        sql = sql + "appointment_id=" + appointment_id + ",";
    }

    if (referencing_id != "NULL") {
        sql = sql + "referencing_id=" + referencing_id;
    }

    var sql = sql + "WHERE id=" + id;

    try {
        var con = mysql.createConnection(db_login);
        con.connect(function(err) {
            if (err) throw err;
            console.log(sql);
            con.query(sql,
                function(err, result, fields) {
                    if (err) throw err;
                    res.json({
                        users: result
                    });
                });
        });
    } catch (err) {
        console.log(err);
    }
});

router.route('/deleteClient').post(function(req, res) {

    var id = parsedJsonData(req.body.id);

    try {
        var con = mysql.createConnection(db_login);
        con.connect(function(err) {
            if (err) throw err;
            con.query("DELETE FROM clients WHERE id=" + id, function(err, result, fields) {
                if (err) throw err;

                res.json({
                    users: result
                });
            });
        });
    } catch (err) {
        console.log(err);
    }
});

app.use('/', router);
app.listen(port);
console.log('Listening on port ' + port);
