router.route('/createAppointment').post(function(req, res) {

    var id = parsedJsonData(req.body.id);
    var client_id = parsedJsonData(req.body.client_id);
    var appointment = parsedJsonData(req.body.appointment);
    var admin_id = parsedJsonData(req.body.admin_id);

    try {
        con.connect(function(err) {
            if (err) throw err;
            con.query("insert into appointments (client_id, appointment, admin_id) values (" +
                client_id + ", " + appointment + ", " + admin_id + ")",
                function(err, result, fields) {
                    if (err) throw err;
                    res.json({
                        users: result
                    });
                });
        });
    } catch (err) {
        console.log(err);
    }
});

router.route('/getAppointments').get(function(req, res) {
    try {
        con.connect(function(err) {
            if (err) throw err;
            con.query("SELECT * FROM appointments", function(err, result, fields) {
                if (err) throw err;
                res.json({
                    users: result
                });
            });
        });
    } catch (err) {
        console.log(err);
    }
});

router.route('/updateAppointment').post(function(req, res) {

    var id = parsedJsonData(req.body.id);
    var client_id = parsedJsonData(req.body.client_id);
    var appointment = parsedJsonData(req.body.appointment);
    var admin_id = parsedJsonData(req.body.admin_id);

    var sql = "UPDATE appointments SET ";

    if (client_id != "NULL") {
        sql = sql + "client_id=" + client_id + ",";
    }

    if (appointment != "NULL") {
        sql = sql + "appointment=" + appointment + ",";
    }

    if (admin_id != "NULL") {
        sql = sql + "admin_id=" + admin_id + ",";
    }

    var sql = sql + "WHERE id=" + id;

    try {
        con.connect(function(err) {
            if (err) throw err;
            console.log(sql);
            con.query(sql,
                function(err, result, fields) {
                    if (err) throw err;
                    res.json({
                        users: result
                    });
                });
        });
    } catch (err) {
        console.log(err);
    }
});

router.route('/deleteAppointment').post(function(req, res) {

    var id = parsedJsonData(req.body.id);

    try {
        con.connect(function(err) {
            if (err) throw err;
            con.query("DELETE FROM appointments WHERE id=" + id, function(err, result, fields) {
                if (err) throw err;

                res.json({
                    users: result
                });
            });
        });
    } catch (err) {
        console.log(err);
    }
});
