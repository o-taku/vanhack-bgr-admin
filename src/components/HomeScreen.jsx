import React from 'react';

const userType =
  window.localStorage.getItem('permission') == 1 ? 'Admin' : 'Agency';

const HomeScreen = () => (
  <div>
    <h1>Welcome {userType} User! </h1>
  </div>
);

export default HomeScreen;
