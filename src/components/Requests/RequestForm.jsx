import React, { Component } from 'react';
import axios from 'axios';
import {
  TextField,
  Button,
  Checkbox,
  FormControlLabel,
  FormGroup,
  Grid
} from '@material-ui/core';

class RequestForm extends Component {
  state = {
    user: {},
    gearList: [],
    chosenGears: [],
    isAdmin: window.localStorage.getItem('permission') == 1,
    id: null
  };

  componentDidMount() {
    axios.get('http://71.19.254.108:8080/getGearCategory').then(({ data }) => {
      this.setState({ gearList: data.gear_category });
    });
  }

  _shouldEnable = () => {
    //   const { user } = this.state;

    //   const {
    //     user: {
    //       firstName,
    //       lastName,
    //       phone,
    //       email,
    //       demographic,
    //       dateOfBirth,
    //       babyDateOfBirth
    //     },
    //     chosenGears
    //   } = this.state;

    //   const hasOneGearItem = chosenGears.length > 0;
    //   return (
    //     hasOneGearItem &&
    //     firstName &&
    //     lastName &&
    //     phone &&
    //     email &&
    //     demographic &&
    //     dateOfBirth &&
    //     babyDateOfBirth
    //   );
    return false;
  };

  _updateUserObject = field => e => {
    const value = e.target.value;
    const updates = { [field]: value };

    this.setState(prevState => ({
      user: {
        ...prevState.user,
        ...updates
      }
    }));
  };

  _submitRequestForm = () => {
    const {
      dateOfBirth,
      babyDateOfBirth,
      demographic,
      firstNameNew,
      lastName,
      phone,
      email
    } = this.state.user;

    const { chosenGear } = this.state;

    const body = {
      firstName: firstNameNew,
      lastName,
      phone,
      email,
      dateOfBirth,
      babyDateOfBirth,
      demographic,
      status: "Pending",
      referencing_id: 3
    };

    console.log(body)
    axios
      .post('http://71.19.254.108:8080/createClient', body)
      .then(response => {
        console.log(response)
        console.log(response.data.users.insertId)
        const OPT = ['1','7','19']
        for (var i = 0; i<3; i++){
          axios.post('http://71.19.254.108:8080/createGear', {"cat_id":OPT[i], "client_id":response.data.users.insertId, "status":"Requested"})
          .then(
            response => {
              console.log(response)
            }
          )
        }
      });
  };

  _denyRequest = () => {
    const { activeUser } = this.state;
    axios
      .get(`http://71.19.254.108:8080/declineClient/${activeUser.id}`)
      .then(response => console.log(response));
  };

  _scheduleAppointment = () => {
    const { activeUser } = this.state;
    const req = {
      client_id: '',
      appointment: '2018-09-27 11:00',
      admin_id: '3'
    };
    axios
      .post('http://71.19.254.108:8080/createAppointment', req)
      .then(response => console.log(response));
    axios
      .post('http://71.19.254.108:8080/sendAppointmentNotification', {
        id: '1'
      })
      .then(response => {console.log(response)});
  };

  _updateChosenGears = e => {
    const gearItem = e.target.value;
    let { chosenGears } = this.state;

    const index = chosenGears.indexOf(gearItem);
    const isInList = index > -1;

    if (isInList) {
      chosenGears.splice(index, 1);
    } else {
      chosenGears = [...chosenGears, gearItem];
    }
    this.setState({
      chosenGears
    });
  };

  _renderCheckboxes = () => {
    const { gearList, chosenGears } = this.state;

    return (
      gearList &&
      gearList.map((gearItem, i) => (
        <FormControlLabel
          key={i}
          control={<Checkbox value={gearItem.name} />}
          label={gearItem.name}
          checked={chosenGears.includes(gearItem.name)}
          onChange={this._updateChosenGears}
        />
      ))
    );
  };

  componentWillUpdate(prevProps, prevState) {
    console.log(prevProps)
    if (this.state.isAdmin && prevState.id !== this.props.id){
      const { id } = this.props;
      console.log(id)
      axios
        .get(`http://71.19.254.108:8080/getGearByClient/${id}`)
        .then(( {data}) =>{
          // console.log(response)
          this.setState({ chosenGears: data.gear.map(gear => gear.name), id })
        }
        );
    }
  }

  render() {
    const isDisabled = false;
    const { activeUser, userGearNeeds } = this.props;
    const { isAdmin } = this.state
    const { user } = this.state;

    if (activeUser && activeUser.firstName !== user.firstName) {
      // user has updated
      this.setState({
        user: activeUser,
        // chosenGears: userGearNeeds
      });
    }

    const {
      dateOfBirth,
      babyDateOfBirth,
      demographic,
      firstName,
      firstNameNew,
      lastName,
      phone,
      email
    } = this.state.user;

    console.log(isAdmin);

    return (
      <div
        className="request-form"
        style={{
          padding: '30px',
          maxWidth: '600px',
          borderRadius: '5px',
          display: 'flex',
          flexDirection: 'column',
          background: '#fff'
        }}
      >
        <h1 style={{ fontFamily: 'Nunito' }}>Client Information</h1>
        {isAdmin && 
          <TextField
          placeholder="Client first name"
          onChange={this._updateUserObject('firstName')}
          value={firstName}
          style={{ padding: '10px 0' }}
          style={{ paddingTop: '30px', marginBottom: '10px' }}
          label="First name"
        />}
        {!isAdmin && 
          <TextField
          placeholder="Client first name"
          onChange={this._updateUserObject('firstNameNew')}
          value={firstNameNew}
          style={{ paddingTop: '30px', marginBottom: '10px' }}
          label="First name"
        />}
        <TextField
          placeholder="Client last name"
          onChange={this._updateUserObject('lastName')}
          value={lastName}
          style={{ padding: '10px 0' }}
          style={{ paddingTop: '30px', marginBottom: '10px' }}
          label="Last name"
        />
        <TextField
          placeholder="Phone number"
          onChange={this._updateUserObject('phone')}
          value={phone}
          style={{ paddingTop: '30px', marginBottom: '10px' }}
          label="Phone numbers"
        />
        <TextField
          placeholder="Email address"
          onChange={this._updateUserObject('email')}
          value={email}
          style={{ paddingTop: '30px', marginBottom: '10px' }}
          label="Email address"
        />
        <TextField
          placeholder="Demographic"
          onChange={this._updateUserObject('demographic')}
          value={demographic}
          style={{ paddingTop: '30px', marginBottom: '10px' }}
          label="Demographic"
        />
        <TextField
          // placeholder="Client DOB"
          onChange={this._updateUserObject('dateOfBirth')}
          value={dateOfBirth}
          style={{ paddingTop: '30px', marginBottom: '10px' }}
          label="Date of Birth"
        />
        <TextField
          placeholder="Baby DOB"
          onChange={this._updateUserObject('babyDateOfBirth')}
          value={babyDateOfBirth}
          style={{ paddingTop: '30px', marginBottom: '10px' }}
          label="Baby DOB"
        />

        <h1 style={{ fontFamily: 'Nunito' }}>Gear Required</h1>
        <FormGroup row>{this._renderCheckboxes()}</FormGroup>
        <Grid container style={{ marginTop: '30px' }}>
          {isAdmin && (
            <Grid item l={7}>
              <Button
                variant="outlined"
                color="primary"
                onClick={this._scheduleAppointment}
              >
                Schedule Appointment
              </Button>
              <Button
                variant="outlined"
                color="secondary"
                onClick={this._denyRequest}
              >
                Deny Request
              </Button>
            </Grid>
          )}
          <Grid item xs={3}>
            <Button
              disabled={isDisabled}
              variant="outlined"
              onClick={this._submitRequestForm}
            >
              {isAdmin ? 'Update Request' : 'Submit Request'}
            </Button>
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default RequestForm;
