import React, { Component } from 'react';
import axios from 'axios';
import RequestForm from './RequestForm';
import { Card, CardContent, Grid } from '@material-ui/core';
import ContactCard from '../Appointment/ContactCard';

class Requests extends Component {
  state = {
    clients: [],
    activeUser: {},
    gearNeeds: [],
    name: 'Cat in the Hat',
    multiline: 'Controlled',
    isAdmin: window.localStorage.getItem('permission') == true // sorry for hardcode
  };

  componentDidMount() {
    axios
      .get('http://71.19.254.108:8080/getClients')
      .then(({ data }) => this.setState({ clients: data.clients }));
  }

  showUserAppointment = userID => () => {
    this.setState({ activeUserID: userID });
    axios
      .get(`http://71.19.254.108:8080/getClientById/${userID}`)
      .then(({ data }) => this.setState({ activeUser: data.clients[0] }));
    axios
      .get(`http://71.19.254.108:8080/getGearByClient/${userID}`)
      .then(({ data }) =>
        this.setState({ gearNeeds: data.gear.map(gear => gear.name) })
      );
  };

  render() {
    const { activeUser, activeUserID, clients, isAdmin, gearNeeds } = this.state;

    console.log(this.state);
    console.log(this.state.isAdmin);

    return (
      <Grid container spacing={8}>
        <Grid item xs={4}>
          {isAdmin &&
            clients &&
            clients.map(client => (
              <ContactCard
                id={client.id}
                key={client.id}
                name={client.firstName}
                autoDenied={client.status === 'Denied'}
                showUserAppointment={this.showUserAppointment(client.id)}
              />
            ))}
        </Grid>
        <Grid item xs={8}>
          {isAdmin &&
            activeUser &&
            !activeUser.firstName && (
              <h1 style={{ textAlign: 'center' }}>
                You have {clients.length} client requests
              </h1>
            )}
          {isAdmin &&
            activeUser &&
            activeUser.firstName && (
              <Card>
                <CardContent>
                  <RequestForm
                    activeUser={activeUser}
                    id={activeUserID}
                    isAdmin
                    userGearNeeds={gearNeeds} // TODO: this needs to be an array
                  />
                </CardContent>
              </Card>
            )}
          {!isAdmin && (
            <Card>
              <CardContent>
                <RequestForm
                  activeUser={activeUser}
                  isAdmin
                  userGearNeeds={gearNeeds} // TODO: this needs to be an array
                />
              </CardContent>
            </Card>
          )}
        </Grid>
      </Grid>
    );
  }
}

export default Requests;
