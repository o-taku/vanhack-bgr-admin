import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

import axios from 'axios'

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
});

class agentTable extends React.Component{
  state = {
    anchorEl: null,
    agent: [],
    aaa:[]
  };

  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  updateAgent = (id, status) => () => {
    axios.post('http://71.19.254.108:8080/getUsers',
    {"id":id, "status":status}
    )
  }

  componentDidMount() {
    console.log(axios.get('http://71.19.254.108:8080/getUsers'))
    axios.get('http://71.19.254.108:8080/getUsers').then(({data}) => {
      console.log(data.users)
      this.setState({ agent: data.users });
    })
  }

  render() {
    return (
      <Paper>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Agency ID</TableCell>
              <TableCell>Name</TableCell>
              <TableCell>Phone</TableCell>
              <TableCell>Email</TableCell>
              <TableCell>Status</TableCell>
              <TableCell>Added By</TableCell>
              <TableCell>Date Created</TableCell>
              <TableCell>Action</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {(this.state.agent).map(agent => {
              return (
                <TableRow key={agent.id}>
                  <TableCell component="th" scope="agent">
                    {agent.Agency_id}
                  </TableCell>
                  <TableCell>{agent.name}</TableCell>
                  <TableCell>{agent.phone}</TableCell>
                  <TableCell>{agent.email}</TableCell>
                  <TableCell>{agent.status}</TableCell>
                  <TableCell>{agent.added_by}</TableCell>
                  <TableCell>{agent.date_created}</TableCell>
                  <TableCell>
                    <IconButton variant="fab" aria-label="done" onClick={this.updateAgent(agent.id,'Approve')}>
                      <Icon>done</Icon>
                    </IconButton>
                    <IconButton variant="fab" aria-label="Edit" onClick={this.updateAgent(agent.id,'Denied')}>
                      <Icon>clear</Icon>
                    </IconButton>
                    <IconButton variant="fab" aria-label="Edit">
                      <Icon>edit_icon</Icon>
                    </IconButton>
                  </TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </Paper>
    );
  }
}

export default agentTable;