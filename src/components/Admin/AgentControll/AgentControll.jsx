import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import Grid from '@material-ui/core/Grid';
import AgentTable from './AgentTable';

class agentControll extends Component {

  render() {
    return (
      <div>
        {/* <Grid container spacing={16}>
          <Grid item xs={12}>
            <Link to="/add_agent">
              <Button variant="fab" color="primary" aria-label="Add" style={{float: 'right'}}>
                <AddIcon />
              </Button>
            </Link>
          </Grid>
        </Grid> */}
        <AgentTable/>
      </div>
    );
  }
}

export default agentControll;
