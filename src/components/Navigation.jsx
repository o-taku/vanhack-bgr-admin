import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Drawer, Divider, Button, List, MenuItem, ListSubheader } from '@material-ui/core';

import { Link } from 'react-router-dom';

const styles = {
  list: {
    width: 250
  },
  fullList: {
    width: 'auto'
  }
};

class TemporaryDrawer extends React.Component {
  state = {
    open: false,
    isAdmin: window.localStorage.getItem('permission') == 1
  };

  toggleDrawer = () => {
    this.setState(prevState => ({
      open: !prevState.open
    }));
  };

  switchUser = () => {
    this.setState({ isAdmin: !this.state.isAdmin });
    localStorage["permission"] = !window.localStorage.getItem('permission');
  }

  render() {
    const { classes } = this.props;

    const { open, isAdmin } = this.state;

    const adminList = (
      <div className={classes.list}>
        <List style={{ marginTop: '30px' }}>
          <Link
            to="/admin/agent_controll"
            style={{ color: '#404040', textDecoration: 'none', display: 'block' }}
          >
            <MenuItem>Agent Controll</MenuItem>
          </Link>
          <Link
            to="/admin/requests"
            style={{ color: '#404040', textDecoration: 'none' }}
          >
            <MenuItem>Requests</MenuItem>
          </Link>
          <Link
            to="/admin/schedule"
            style={{ color: '#404040', textDecoration: 'none' }}
          >
            <MenuItem>Schedule</MenuItem>
          </Link>
          <Link
            to="/admin/gear"
            style={{ color: '#404040', textDecoration: 'none' }}
          >
            <MenuItem>Gear</MenuItem>
          </Link>
        </List>
      </div>
    );

    const agencyList = (
      <div className={classes.list}>
        <List style={{ marginTop: '30px' }}>
          <Link
            to="/agent/add_agent"
            style={{ color: '#404040', textDecoration: 'none' }}
          >
            <MenuItem>Add Agent</MenuItem>
          </Link>
          <Link
            to="/agent/requests"
            style={{ color: '#404040', textDecoration: 'none' }}
          >
            <MenuItem>Requests</MenuItem>
          </Link>
        </List>
      </div>
    );

    return (
      <div>
        <Button onClick={this.toggleDrawer}>Menu</Button>
        <Button onClick={this.switchUser}>Current User: {isAdmin ? 'Admin' : 'Agent'}</Button>
        <Drawer open={open} onClose={this.toggleDrawer}>
          <div
            tabIndex={0}
            role="button"
            onClick={this.toggleDrawer}
            onKeyDown={this.toggleDrawer}
          >
            {isAdmin ? adminList : agencyList}
          </div>
        </Drawer>
      </div>
    );
  }
}

TemporaryDrawer.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(TemporaryDrawer);
