import React, { Component } from 'react';
import { Grid } from '@material-ui/core';
import GearItem from './GearItem';
import axios from 'axios';

class Gear extends Component {
  state = {};

  componentDidMount() {
    axios.get('http://71.19.254.108:8080/getGearCategory').then(({ data }) => {
      this.setState({ gearList: data.gear_category });
    });
  }
  render() {
    const { gearList } = this.state;

    return (
      <div>
        <h1 style={{ textAlign: 'center' }}>Gear</h1>
        <Grid container>
          {gearList &&
            gearList.map((gearItem, i) => (
              <Grid item xs={4} style={{ padding: '20px' }}>
                <GearItem name={gearItem.name} key={i} />
              </Grid>
            ))}
        </Grid>
      </div>
    );
  }
}

export default Gear;
