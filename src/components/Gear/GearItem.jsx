import React, { Component } from 'react';
import { Card, CardContent } from '@material-ui/core';

class GearItem extends Component {
  render() {
    const { name } = this.props;
    return (
      <Card>
        <CardContent>
          <h2>{name}</h2>
        </CardContent>
      </Card>
    );
  }
}

export default GearItem;
