import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import ButtonBase from '@material-ui/core/ButtonBase';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

const styles = {
  card: {
    minWidth: 275
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)'
  },
  title: {
    marginBottom: 16,
    fontSize: 14
  },
  pos: {
    marginBottom: 12
  }
};

function AddCard({ classes, showUserAppointment }) {
  return (
    <ButtonBase onClick={showUserAppointment} style={{marginTop: '10px'}}>
      <Card className={classes.card}>
        <CardContent>
          <Grid container spacing={8}>
            <Grid item xs={4}>
              <Avatar className={classes.avatar}>+</Avatar>
            </Grid>
            <Grid item xs={8}>
              <Typography variant="title" align="left">
                Add New
              </Typography>
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    </ButtonBase>
  );
}

AddCard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(AddCard);
