import React, { Component } from 'react';
import Calendar from 'react-calendar';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';
import AddCard from './AddCard'
import ContactCard from './ContactCard';
import RequestForm from '../Requests/RequestForm';

class Appointment extends Component {
  state = {
    activeUser: ''
  };

  render() {
    return (
      <div>
        <iframe
          src="https://calendar.google.com/calendar/embed?height=600&amp;wkst=1&amp;bgcolor=%23FFFFFF&amp;src=h1ja1v86h077t6dpvhlco26u54%40group.calendar.google.com&amp;color=%23AB8B00&amp;ctz=America%2FLos_Angeles"
          style={{ borderWidth: 0 }}
          width="800"
          height="600"
          frameborder="0"
          scrolling="no"
        />
        <br />
        <a
          target="_blank"
          href="https://calendar.google.com/event?action=TEMPLATE&amp;tmeid=NWxsa2VyOGVnbmRzc3RsNGFlcTRuNTIwMWQgaDFqYTF2ODZoMDc3dDZkcHZobGNvMjZ1NTRAZw&amp;tmsrc=h1ja1v86h077t6dpvhlco26u54%40group.calendar.google.com"
        >
          <img
            border="0"
            src="https://www.google.com/calendar/images/ext/gc_button1_en.gif"
          />
        </a>
      </div>
    );
  }

  // showUserAppointment = userObject => () => {
  //   this.setState({ activeUser: userObject });
  //   axios.get;
  // };

  // render() {
  //   const { activeUser } = this.state;

  //   console.log('activeUSer', activeUser);

  //   return (
  //     <Grid container spacing={8}>
  //       <Grid item xs={4}>
  //         <ContactCard
  //           name="Carly" // TODO: USE DATABASE
  //           showUserAppointment={this.showUserAppointment(USERS.carlyRequest)} // TODO: USE DATABASE
  //         />
  //         <ContactCard
  //           name="Sachiko" // TODO: USE DATABASE
  //           showUserAppointment={this.showUserAppointment(USERS.sachikoRequest)} // TODO: USE DATABASE
  //         />
  //       </Grid>
  //       <Grid item xs={8}>
  //         <Card>
  //           <CardContent>
  //             <RequestForm
  //               activeUser={activeUser.user}
  //               userGearNeeds={activeUser.gear}
  //             />
  //           </CardContent>
  //         </Card>
  //       </Grid>
  //     </Grid>
  //   );
  // }
}

export default Appointment;
