import React, { Component } from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import Appointment from './components/Appointment/Appointment';
import Requests from './components/Requests/Requests';
import AgentControll from './components/Admin/AgentControll/AgentControll';
// import ScheduleControll from './components/Admin/ScheduleControll/ScheduleControll';
import AddAgent from './components/AddAgent/AddAgent';
import Gear from './components/Gear/Gear';
import App from './App';
import HomeScreen from './components/HomeScreen';

class Router extends Component {
  render() {
    return (
      <BrowserRouter>
        <App>
          <Switch>
            <Route exact path="/admin/agent_controll" component={AgentControll} />
            <Route exact path="/admin/schedule" component={Appointment} />
            <Route exact path="/admin/requests" component={Requests} />
            <Route exact path="/admin/gear" component={Gear} />
            <Route exact path="/agent/add_agent" component={AddAgent} />
            <Route exact path="/agent/schedule" component={Appointment} />
            <Route exact path="/agent/requests" component={Requests} />
            <Route exact path="/" component={HomeScreen} />
            <Route path="*" component={() => <Redirect to="/" />} />
          </Switch>
        </App>
      </BrowserRouter>
    );
  }
}

export default Router;
